from pyvcloud.vcd.client import Client, BasicLoginCredentials, QueryResultFormat, EntityType, ResourceType
from pyvcloud.vcd.utils import to_dict
from pyvcloud.vcd.gateway import Gateway
import requests
import logging
import json
import argparse

requests.packages.urllib3.disable_warnings()


class VcdExport(object):
    def __init__(self, user: str, org: str, password: str, host: str, client: Client = None, verify: bool = False):
        if client is None:
            self.client = Client(host, verify_ssl_certs=verify)
            self.client.set_credentials(BasicLoginCredentials(user=user, org=org, password=password))
        self.client.set_highest_supported_version()
        self.vdcs = []

    def get_vdc_metrics(self):
        logger.debug("Running vDC metrics Query")
        vdc_query = self.client.get_typed_query(query_type_name=ResourceType.ADMIN_ORG_VDC.value,
                                                query_result_format=QueryResultFormat.ID_RECORDS,
                                                include_links=True)
        vdcs = list(vdc_query.execute())
        for vdc in vdcs:
            temp = to_dict(obj=vdc, resource_type=EntityType.RECORDS.value)
            self.vdcs.append(temp)

    def get_vm_metrics(self):
        for vdc in self.vdcs:
            logger.debug("Running vm metrics query for vDC(%s)" % vdc['name'])
            vm_query = self.client.get_typed_query(query_type_name=ResourceType.VM.value,
                                                   query_result_format=QueryResultFormat.ID_RECORDS,
                                                   qfilter="isVAppTemplate==false,vdc==" + vdc['id'],
                                                   include_links=True)
            vms = list(vm_query.execute())
            vdc['vms'] = []
            for vm in vms:
                temp = to_dict(obj=vm, resource_type=EntityType.RECORDS.value)
                vdc['vms'].append(temp)

    def get_edge_metrics(self):
        for vdc in self.vdcs:
            logger.debug("Running edge query for vDC(%s)" % vdc['name'])
            edge_query = self.client.get_typed_query(query_type_name=ResourceType.EDGE_GATEWAY.value,
                                                     query_result_format=QueryResultFormat.ID_RECORDS,
                                                     qfilter="vdc==" + vdc['id'],
                                                     include_links=True)
            edges = list(edge_query.execute())
            vdc['edges'] = []
            for edge in edges:
                temp = to_dict(obj=edge, resource_type=EntityType.RECORDS.value)
                edge_refs_query = self.client.get_typed_query(query_type_name=ResourceType.EDGE_GATEWAY.value,
                                                              query_result_format=QueryResultFormat.REFERENCES,
                                                              qfilter="id==" + temp['id'],
                                                              include_links=True)
                edge_refs = list(edge_refs_query.execute())
                for edge_ref in edge_refs:
                    try:
                        edge_obj = Gateway(client=self.client, name=edge_ref.attrib['name'], resource=edge_ref)
                        edge_obj.reload_admin()
                        temp['edgeConfigurationType'] = str(edge_obj.admin_resource.Configuration.GatewayBackingConfig)
                    except Exception as e:
                        logger.warning("API Error reloading admin configuration on Edge(%s): %s" %
                                       (edge_ref.attrib['name'], e))
                        temp['edgeConfigurationType'] = "UNKNOWN"
                        continue
                vdc['edges'].append(temp)

    def get_network_metrics(self):
        for vdc in self.vdcs:
            logger.debug("Running network query for vDC(%s)" % vdc['name'])
            network_query = self.client.get_typed_query(query_type_name=ResourceType.ORG_VDC_NETWORK.value,
                                                        query_result_format=QueryResultFormat.ID_RECORDS,
                                                        qfilter="vdc==" + vdc['id'],
                                                        include_links=True)
            networks = list(network_query.execute())
            vdc['networks'] = []
            for network in networks:
                temp = to_dict(obj=network, resource_type=EntityType.RECORDS.value)
                vdc['networks'].append(temp)

    def get_storage_metrics(self):
        for vdc in self.vdcs:
            logger.debug("Running storage query for vDC(%s)" % vdc['name'])
            storage_query = self.client.get_typed_query(query_type_name=ResourceType.ADMIN_ORG_VDC_STORAGE_PROFILE.value,
                                                        query_result_format=QueryResultFormat.ID_RECORDS,
                                                        qfilter="vdc==" + vdc['id'],
                                                        include_links=True)
            storages = list(storage_query.execute())
            vdc['storage'] = []
            for storage in storages:
                temp = to_dict(obj=storage, resource_type=EntityType.RECORDS.value)
                vdc['storage'].append(temp)


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(name)s : %(message)s')
    logger = logging.getLogger('vcd')
    logger.setLevel(logging.DEBUG)
    parser = argparse.ArgumentParser()
    parser.add_argument("-user", help="vCD Username")
    parser.add_argument("-org", default="System", help="vCD Users Organization")
    parser.add_argument("-passwd", help="vCD Password")
    parser.add_argument("-host", help="vCD Instance url")
    args = parser.parse_args()

    vcd = VcdExport(user=args.user, org=args.org, password=args.passwd, host=args.host)
    vcd.get_vdc_metrics()
    vcd.get_vm_metrics()
    vcd.get_edge_metrics()
    vcd.get_network_metrics()
    vcd.get_storage_metrics()
    with open('results', 'w') as cache:
        json.dump(vcd.vdcs, cache)
